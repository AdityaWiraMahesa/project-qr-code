import 'package:flutter/material.dart';
import 'package:project_scan_qr/url.dart';
import 'package:qrscan/qrscan.dart' as scanner;

class qrcode extends StatefulWidget {
  @override
  _qrcodeState createState() => _qrcodeState();
}

class _qrcodeState extends State<qrcode> {
  String text = "Tap Here to Scan QR Code!";
  Color _setColor = Colors.white;
  // String btn = "Scan";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 70,
        ),
        Container(
          // color: Colors.amber,
          child: Column(
            children: [
              Icon(Icons.qr_code, size: 135),
              Text(
                "QR CODE SCANNER",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 100,
        ),
        TextButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => UrlResult(text: text)));
            },
            child: Text(
              text,
              style: TextStyle(fontSize: 20, color: _setColor),
              textAlign: TextAlign.center,
            )),
        SizedBox(height: 15),
        RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(80),
          ),
          onPressed: () async {
            text = await scanner.scan();
            _setColor = Colors.blue;
            setState(() {});
          },
          child: Container(
            width: 80,
            height: 30,
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.qr_code_outlined),
                SizedBox(width: 5),
                Text(
                  "Scan",
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 200,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Develop with "),
            Icon(
              Icons.favorite,
              color: Colors.pink,
            ),
            Text(
              " By Aditya Wira Mahesa",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        )
      ],
    )));
  }
}
